#import "TLReading.h"

@implementation TLReading

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"probeId": @"probe_id", @"probeName": @"probe_name", @"fieldId": @"field_id", @"timestamp": @"timestamp", @"location": @"location", @"operation": @"operation", @"readings": @"readings" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"probeId", @"probeName", @"fieldId", @"timestamp", @"location", @"operation", @"readings"];
  return [optionalProperties containsObject:propertyName];
}

@end
