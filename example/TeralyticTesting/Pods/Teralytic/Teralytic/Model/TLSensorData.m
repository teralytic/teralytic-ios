#import "TLSensorData.h"

@implementation TLSensorData

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"depth": @"depth", @"depthUnits": @"depth_units", @"terascore": @"terascore", @"nitrogen": @"nitrogen", @"nitrogenPpm": @"nitrogen_ppm", @"phosphorus": @"phosphorus", @"phosphorusPpm": @"phosphorus_ppm", @"potassium": @"potassium", @"potassiumPpm": @"potassium_ppm", @"ec": @"ec", @"o2": @"o2", @"pH": @"pH", @"co2": @"co2", @"awc": @"awc", @"soilMoisture": @"soil_moisture", @"soilTemperature": @"soil_temperature", @"soilTexture": @"soil_texture", @"extendedAttributes": @"extended_attributes" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"depth", @"depthUnits", @"terascore", @"nitrogen", @"nitrogenPpm", @"phosphorus", @"phosphorusPpm", @"potassium", @"potassiumPpm", @"ec", @"o2", @"pH", @"co2", @"awc", @"soilMoisture", @"soilTemperature", @"soilTexture", @"extendedAttributes"];
  return [optionalProperties containsObject:propertyName];
}

@end
