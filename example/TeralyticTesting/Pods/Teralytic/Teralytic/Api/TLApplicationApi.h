#import <Foundation/Foundation.h>
#import "TLApplication.h"
#import "TLError.h"
#import "TLApi.h"

/**
* teralytic
* The Teralytic API allows clients to manage their organization, view their fields and and probes, and query sensor readings and analytics.  For sandbox testing you may use the api key: `swagger.teralytic.io` 
*
* OpenAPI spec version: 1.0.6
* 
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/



@interface TLApplicationApi: NSObject <TLApi>

extern NSString* kTLApplicationApiErrorDomain;
extern NSInteger kTLApplicationApiMissingParamErrorCode;

-(instancetype) initWithApiClient:(TLApiClient *)apiClient NS_DESIGNATED_INITIALIZER;

/// Create a new organization client with the specified scope
/// Create a new third-party application client
///
/// @param organizationId id of Organization for the operation
/// @param application The application to create (optional)
/// 
///  code:201 message:"Created",
///  code:400 message:"BadRequest",
///  code:401 message:"Unauthorized",
///  code:404 message:"ResourceNotFound",
///  code:500 message:"ServerError"
///
/// @return TLApplication*
-(NSURLSessionTask*) applicationCreateWithOrganizationId: (NSString*) organizationId
    application: (TLApplication*) application
    completionHandler: (void (^)(TLApplication* output, NSError* error)) handler;


/// Delete an application api client
/// Delete an application client for the organization from the database
///
/// @param organizationId id of Organization for the operation
/// @param applicationId id of application to delete
/// 
///  code:204 message:"No Content",
///  code:401 message:"Unauthorized",
///  code:404 message:"ResourceNotFound",
///  code:500 message:"ServerError"
///
/// @return void
-(NSURLSessionTask*) applicationDeleteWithOrganizationId: (NSString*) organizationId
    applicationId: (NSString*) applicationId
    completionHandler: (void (^)(NSError* error)) handler;


/// Get organization applications
/// List all applications for an organization
///
/// @param organizationId id of Organization for the operation
/// 
///  code:200 message:"OK",
///  code:401 message:"Unauthorized",
///  code:404 message:"ResourceNotFound",
///  code:500 message:"ServerError"
///
/// @return NSArray<TLApplication>*
-(NSURLSessionTask*) applicationListWithOrganizationId: (NSString*) organizationId
    completionHandler: (void (^)(NSArray<TLApplication>* output, NSError* error)) handler;



@end
