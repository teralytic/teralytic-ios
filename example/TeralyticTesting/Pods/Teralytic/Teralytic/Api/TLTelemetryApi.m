#import "TLTelemetryApi.h"
#import "TLQueryParamCollection.h"
#import "TLApiClient.h"
#import "TLError.h"
#import "TLReading.h"


@interface TLTelemetryApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation TLTelemetryApi

NSString* kTLTelemetryApiErrorDomain = @"TLTelemetryApiErrorDomain";
NSInteger kTLTelemetryApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[TLApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(TLApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Query field anlaytics
/// List single Field details associated with Field id provided (from set of Fields associated with the account key)
///  @param organizationId id of Organization to retrieve 
///
///  @param fieldId id of Field to retrieve 
///
///  @param startDate Start date and time for the query in RFC3339 format (optional)
///
///  @param endDate End date and time for the query in RFC3339 format (optional)
///
///  @param operation Operation to perform on reading data (optional, default to mean)
///
///  @param sampleRate The operation sample interval, i.e. `15m` (optional, default to 15m)
///
///  @param limit limit the number of returns per depth (optional)
///
///  @param extended Return extended attributes (optional, default to false)
///
///  @param sort return a sorted array, will decrease performance (optional, default to true)
///
///  @returns NSArray<TLReading>*
///
-(NSURLSessionTask*) fieldAnalyticsQueryWithOrganizationId: (NSString*) organizationId
    fieldId: (NSString*) fieldId
    startDate: (NSDate*) startDate
    endDate: (NSDate*) endDate
    operation: (NSString*) operation
    sampleRate: (NSString*) sampleRate
    limit: (NSNumber*) limit
    extended: (NSNumber*) extended
    sort: (NSNumber*) sort
    completionHandler: (void (^)(NSArray<TLReading>* output, NSError* error)) handler {
    // verify the required parameter 'organizationId' is set
    if (organizationId == nil) {
        NSParameterAssert(organizationId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"organizationId"] };
            NSError* error = [NSError errorWithDomain:kTLTelemetryApiErrorDomain code:kTLTelemetryApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'fieldId' is set
    if (fieldId == nil) {
        NSParameterAssert(fieldId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"fieldId"] };
            NSError* error = [NSError errorWithDomain:kTLTelemetryApiErrorDomain code:kTLTelemetryApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/organizations/{organization_id}/fields/{field_id}/analytics"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (organizationId != nil) {
        pathParams[@"organization_id"] = organizationId;
    }
    if (fieldId != nil) {
        pathParams[@"field_id"] = fieldId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (startDate != nil) {
        queryParams[@"start_date"] = startDate;
    }
    if (endDate != nil) {
        queryParams[@"end_date"] = endDate;
    }
    if (operation != nil) {
        queryParams[@"operation"] = operation;
    }
    if (sampleRate != nil) {
        queryParams[@"sample_rate"] = sampleRate;
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    if (extended != nil) {
        queryParams[@"extended"] = [extended isEqual:@(YES)] ? @"true" : @"false";
    }
    if (sort != nil) {
        queryParams[@"sort"] = [sort isEqual:@(YES)] ? @"true" : @"false";
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"ApiKeyAuth", @"OAuth2"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<TLReading>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<TLReading>*)data, error);
                                }
                            }];
}

///
/// Query probe analytics
/// List single Probe details, including Reading data, associated with Probe id provided (from set of Fields associated with the account key)
///  @param organizationId id of Organization to retrieve 
///
///  @param probeId id of Probe to retrieve 
///
///  @param startDate Start date and time for the query in RFC3339 format (optional)
///
///  @param endDate End date and time for the query in RFC3339 format (optional)
///
///  @param operation Operation to perform on reading data (optional, default to mean)
///
///  @param sampleRate The operation sample interval, i.e. `15m` (optional, default to 15m)
///
///  @param limit limit the number of returns per depth (optional)
///
///  @param extended Return extended attributes (optional, default to false)
///
///  @param sort return a sorted array, will decrease performance (optional, default to true)
///
///  @returns NSArray<TLReading>*
///
-(NSURLSessionTask*) probeAnalyticsQueryWithOrganizationId: (NSString*) organizationId
    probeId: (NSString*) probeId
    startDate: (NSDate*) startDate
    endDate: (NSDate*) endDate
    operation: (NSString*) operation
    sampleRate: (NSString*) sampleRate
    limit: (NSNumber*) limit
    extended: (NSNumber*) extended
    sort: (NSNumber*) sort
    completionHandler: (void (^)(NSArray<TLReading>* output, NSError* error)) handler {
    // verify the required parameter 'organizationId' is set
    if (organizationId == nil) {
        NSParameterAssert(organizationId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"organizationId"] };
            NSError* error = [NSError errorWithDomain:kTLTelemetryApiErrorDomain code:kTLTelemetryApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'probeId' is set
    if (probeId == nil) {
        NSParameterAssert(probeId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"probeId"] };
            NSError* error = [NSError errorWithDomain:kTLTelemetryApiErrorDomain code:kTLTelemetryApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/organizations/{organization_id}/probes/{probe_id}/analytics"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (organizationId != nil) {
        pathParams[@"organization_id"] = organizationId;
    }
    if (probeId != nil) {
        pathParams[@"probe_id"] = probeId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (startDate != nil) {
        queryParams[@"start_date"] = startDate;
    }
    if (endDate != nil) {
        queryParams[@"end_date"] = endDate;
    }
    if (operation != nil) {
        queryParams[@"operation"] = operation;
    }
    if (sampleRate != nil) {
        queryParams[@"sample_rate"] = sampleRate;
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    if (extended != nil) {
        queryParams[@"extended"] = [extended isEqual:@(YES)] ? @"true" : @"false";
    }
    if (sort != nil) {
        queryParams[@"sort"] = [sort isEqual:@(YES)] ? @"true" : @"false";
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"ApiKeyAuth", @"OAuth2"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<TLReading>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<TLReading>*)data, error);
                                }
                            }];
}

///
/// Query sensor readings
/// Query sensor readings associated with organization
///  @param organizationId id of Organization to retrieve 
///
///  @param startDate Start date and time for the query in RFC3339 format, the default is 24h prior  (optional)
///
///  @param endDate End date and time for the query in RFC3339 format, the implied default is now  (optional)
///
///  @param fields The fields to return readings for (optional)
///
///  @param probes The probes to return readings for (optional)
///
///  @param properties The properties to return readings for (optional)
///
///  @param extended Return extended attributes (optional, default to false)
///
///  @returns NSArray<TLReading>*
///
-(NSURLSessionTask*) readingsQueryWithOrganizationId: (NSString*) organizationId
    startDate: (NSDate*) startDate
    endDate: (NSDate*) endDate
    fields: (NSArray<NSString*>*) fields
    probes: (NSArray<NSString*>*) probes
    properties: (NSArray<NSString*>*) properties
    extended: (NSNumber*) extended
    completionHandler: (void (^)(NSArray<TLReading>* output, NSError* error)) handler {
    // verify the required parameter 'organizationId' is set
    if (organizationId == nil) {
        NSParameterAssert(organizationId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"organizationId"] };
            NSError* error = [NSError errorWithDomain:kTLTelemetryApiErrorDomain code:kTLTelemetryApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/organizations/{organization_id}/readings"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (organizationId != nil) {
        pathParams[@"organization_id"] = organizationId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (startDate != nil) {
        queryParams[@"start_date"] = startDate;
    }
    if (endDate != nil) {
        queryParams[@"end_date"] = endDate;
    }
    if (fields != nil) {
        queryParams[@"fields"] = [[TLQueryParamCollection alloc] initWithValuesAndFormat: fields format: @"csv"];
    }
    if (probes != nil) {
        queryParams[@"probes"] = [[TLQueryParamCollection alloc] initWithValuesAndFormat: probes format: @"csv"];
    }
    if (properties != nil) {
        queryParams[@"properties"] = [[TLQueryParamCollection alloc] initWithValuesAndFormat: properties format: @"csv"];
    }
    if (extended != nil) {
        queryParams[@"extended"] = [extended isEqual:@(YES)] ? @"true" : @"false";
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"ApiKeyAuth", @"OAuth2"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<TLReading>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<TLReading>*)data, error);
                                }
                            }];
}



@end
