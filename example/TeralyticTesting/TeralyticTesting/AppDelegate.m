//
//  AppDelegate.m
//  TeralyticTesting
//
//  Created by Rob Rodriguez on 12/17/18.
//  Copyright © 2018 Rob Rodriguez. All rights reserved.
//

#import "AppDelegate.h"

#import <Teralytic/TLApiClient.h>
#import <Teralytic/TLDefaultConfiguration.h>
// load models
#import <Teralytic/TLApplication.h>
#import <Teralytic/TLError.h>
#import <Teralytic/TLField.h>
#import <Teralytic/TLFieldGeometry.h>
#import <Teralytic/TLFieldGeometryGeometry.h>
#import <Teralytic/TLOrgRole.h>
#import <Teralytic/TLOrganization.h>
#import <Teralytic/TLProbe.h>
#import <Teralytic/TLPropertyRole.h>
#import <Teralytic/TLReading.h>
#import <Teralytic/TLReadingData.h>
#import <Teralytic/TLReadingLocation.h>
#import <Teralytic/TLReadingType.h>
#import <Teralytic/TLRolePermission.h>
#import <Teralytic/TLStringArray.h>
#import <Teralytic/TLUUIDArray.h>
#import <Teralytic/TLUser.h>
#import <Teralytic/TLMicroClimateData.h>
#import <Teralytic/TLSensorData.h>
#import <Teralytic/TLWeatherData.h>
// load API classes for accessing endpoints
#import <Teralytic/TLApplicationApi.h>
#import <Teralytic/TLFieldApi.h>
#import <Teralytic/TLOrganizationApi.h>
#import <Teralytic/TLProbeApi.h>
#import <Teralytic/TLTelemetryApi.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];
    
    // Configure API key authorization: (authentication scheme: ApiKeyAuth)
    [apiConfig setApiKey:@"API_KEY" forApiKeyIdentifier:@"x-api-key"];
    // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
    //[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];
    
    // set the host
    [apiConfig setHost: @"https://api.teralytic.io/beta/v1"];
    
    // Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
    [apiConfig setAccessToken:@"ACCESS_TOKEN"];
    
    
    NSString* organizationId = @"ORG_ID"; // id of Organization for the operation
    
    TLTelemetryApi *apiInstance = [[TLTelemetryApi alloc] init];
    
    // Create a new organization client with the specified scope
    [apiInstance readingsQueryWithOrganizationId:organizationId
                                       startDate:nil
                                         endDate:nil
                                          fields:nil
                                          probes:nil
                                      properties:nil
                                        extended:nil
                               completionHandler: ^(NSArray<TLReading>* output, NSError* error) {
                                   for (TLReading *r in output) {
                                       for (TLReadingData *d in r.readings) {
                                           if ([d isKindOfClass:[TLSensorData class]]) {
                                               TLSensorData *sd = (TLSensorData*)d;
                                               NSLog(@"ph = %f", [[sd pH] floatValue]);
                                           }
                                       }
                                   }
                               }];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
