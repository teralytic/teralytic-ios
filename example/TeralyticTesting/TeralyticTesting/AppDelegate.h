//
//  AppDelegate.h
//  TeralyticTesting
//
//  Created by Rob Rodriguez on 12/17/18.
//  Copyright © 2018 Rob Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

