# TLReadingLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**latitude** | **NSNumber*** |  | [optional] 
**longitude** | **NSNumber*** |  | [optional] 
**geohash** | **NSString*** | location geohash (12 digit precision) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


