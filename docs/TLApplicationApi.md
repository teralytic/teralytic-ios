# TLApplicationApi

All URIs are relative to *https://api.teralytic.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**applicationCreate**](TLApplicationApi.md#applicationcreate) | **POST** /organizations/{organization_id}/applications | Create a new organization client with the specified scope
[**applicationDelete**](TLApplicationApi.md#applicationdelete) | **DELETE** /organizations/{organization_id}/applications/{application_id} | Delete an application api client
[**applicationList**](TLApplicationApi.md#applicationlist) | **GET** /organizations/{organization_id}/applications | Get organization applications


# **applicationCreate**
```objc
-(NSURLSessionTask*) applicationCreateWithOrganizationId: (NSString*) organizationId
    application: (TLApplication*) application
        completionHandler: (void (^)(TLApplication* output, NSError* error)) handler;
```

Create a new organization client with the specified scope

Create a new third-party application client

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization for the operation
TLApplication* application = [[TLApplication alloc] init]; // The application to create (optional)

TLApplicationApi*apiInstance = [[TLApplicationApi alloc] init];

// Create a new organization client with the specified scope
[apiInstance applicationCreateWithOrganizationId:organizationId
              application:application
          completionHandler: ^(TLApplication* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLApplicationApi->applicationCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization for the operation | 
 **application** | [**TLApplication***](TLApplication.md)| The application to create | [optional] 

### Return type

[**TLApplication***](TLApplication.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **applicationDelete**
```objc
-(NSURLSessionTask*) applicationDeleteWithOrganizationId: (NSString*) organizationId
    applicationId: (NSString*) applicationId
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete an application api client

Delete an application client for the organization from the database

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization for the operation
NSString* applicationId = @"applicationId_example"; // id of application to delete

TLApplicationApi*apiInstance = [[TLApplicationApi alloc] init];

// Delete an application api client
[apiInstance applicationDeleteWithOrganizationId:organizationId
              applicationId:applicationId
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling TLApplicationApi->applicationDelete: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization for the operation | 
 **applicationId** | [**NSString***](.md)| id of application to delete | 

### Return type

void (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **applicationList**
```objc
-(NSURLSessionTask*) applicationListWithOrganizationId: (NSString*) organizationId
        completionHandler: (void (^)(NSArray<TLApplication>* output, NSError* error)) handler;
```

Get organization applications

List all applications for an organization

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization for the operation

TLApplicationApi*apiInstance = [[TLApplicationApi alloc] init];

// Get organization applications
[apiInstance applicationListWithOrganizationId:organizationId
          completionHandler: ^(NSArray<TLApplication>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLApplicationApi->applicationList: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization for the operation | 

### Return type

[**NSArray<TLApplication>***](TLApplication.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

