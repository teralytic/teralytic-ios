# TLUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSString*** | The users uuid | [optional] 
**organizations** | [**TLUUIDArray***](TLUUIDArray.md) |  | [optional] 
**firstName** | **NSString*** | The user&#39;s first name | [optional] 
**lastName** | **NSString*** | The user&#39;s last name | [optional] 
**email** | **NSString*** | The user&#39;s email address | [optional] 
**emailVerified** | **NSNumber*** | Set if the user&#39;s email address is verified | [optional] 
**username** | **NSString*** | The users login name | [optional] 
**orgRoles** | [**NSArray&lt;TLOrgRole&gt;***](TLOrgRole.md) |  | [optional] 
**propertyRoles** | [**NSArray&lt;TLPropertyRole&gt;***](TLPropertyRole.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


