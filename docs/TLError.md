# TLError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSString*** | The error code | [optional] 
**message** | **NSString*** | The error message | [optional] 
**detail** | **NSDictionary&lt;NSString*, NSString*&gt;*** | The error details | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


