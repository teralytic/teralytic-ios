# TLMicroClimateData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**humidity** | **NSNumber*** |  | [optional] 
**irrigationSched** | **NSNumber*** | Irrigation Schedule | [optional] 
**irrigationVolAcreIn** | **NSNumber*** | Irrigation volume in3/acre | [optional] 
**irrigationVolAcreCm** | **NSNumber*** | Irrigation volume c3/acre | [optional] 
**lux** | **NSNumber*** |  | [optional] 
**irrigationGrossIn** | **NSNumber*** | Gross irrigation in in3 | [optional] 
**irrigationNetIn** | **NSNumber*** | Net irrigation in in3 | [optional] 
**irrigationGrossCm** | **NSNumber*** | Gross irrigation in cm3 | [optional] 
**irrigationNetCm** | **NSNumber*** | Net irrigation in cm3 | [optional] 
**irrigationRec** | **NSString*** | Irrigation recommendation | [optional] 
**temperature** | **NSNumber*** | Air Temperature | [optional] 
**eto** | **NSNumber*** | Evaporative Transpiration | [optional] 
**irrigationStream** | **NSNumber*** |  | [optional] 
**organicMatter** | **NSNumber*** | Organic Matter | [optional] 
**awIn6** | **NSNumber*** | available water at 6 inches | [optional] 
**awIn18** | **NSNumber*** | available water at 18 inches | [optional] 
**awIn36** | **NSNumber*** | available water at 36 inches | [optional] 
**awTotal** | **NSNumber*** | total available water | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


