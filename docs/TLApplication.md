# TLApplication

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSString*** |  | [optional] 
**organizationId** | **NSString*** | The organization this application belongs to | [optional] 
**name** | **NSString*** | The application name | [optional] 
**clientId** | **NSString*** | The client id | [optional] 
**clientSecret** | **NSString*** | The client secret | [optional] 
**scope** | [**TLStringArray***](TLStringArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


