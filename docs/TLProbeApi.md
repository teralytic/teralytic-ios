# TLProbeApi

All URIs are relative to *https://api.teralytic.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**probeGet**](TLProbeApi.md#probeget) | **GET** /organizations/{organization_id}/probes/{probe_id} | List single Probe details, including Reading data, associated with Probe id provided (from set of Fields associated with the account key)
[**probeList**](TLProbeApi.md#probelist) | **GET** /organizations/{organization_id}/probes | List all Probes associated with account key


# **probeGet**
```objc
-(NSURLSessionTask*) probeGetWithOrganizationId: (NSString*) organizationId
    probeId: (NSString*) probeId
        completionHandler: (void (^)(TLProbe* output, NSError* error)) handler;
```

List single Probe details, including Reading data, associated with Probe id provided (from set of Fields associated with the account key)

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization to retrieve
NSString* probeId = @"probeId_example"; // id of Probe to retrieve

TLProbeApi*apiInstance = [[TLProbeApi alloc] init];

// List single Probe details, including Reading data, associated with Probe id provided (from set of Fields associated with the account key)
[apiInstance probeGetWithOrganizationId:organizationId
              probeId:probeId
          completionHandler: ^(TLProbe* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLProbeApi->probeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization to retrieve | 
 **probeId** | **NSString***| id of Probe to retrieve | 

### Return type

[**TLProbe***](TLProbe.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **probeList**
```objc
-(NSURLSessionTask*) probeListWithOrganizationId: (NSString*) organizationId
    fields: (NSArray<NSString*>*) fields
        completionHandler: (void (^)(NSArray<TLProbe>* output, NSError* error)) handler;
```

List all Probes associated with account key

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization to retrieve
NSArray<NSString*>* fields = @[@"fields_example"]; // Fields to filter search on (optional)

TLProbeApi*apiInstance = [[TLProbeApi alloc] init];

// List all Probes associated with account key
[apiInstance probeListWithOrganizationId:organizationId
              fields:fields
          completionHandler: ^(NSArray<TLProbe>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLProbeApi->probeList: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization to retrieve | 
 **fields** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Fields to filter search on | [optional] 

### Return type

[**NSArray<TLProbe>***](TLProbe.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

