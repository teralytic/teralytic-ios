# TLPropertyRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSString*** | Role id | [optional] 
**userId** | **NSString*** | The user id | [optional] 
**propertyId** | **NSString*** | The property id | [optional] 
**permissionIds** | [**TLUUIDArray***](TLUUIDArray.md) |  | [optional] 
**permissions** | [**NSArray&lt;TLRolePermission&gt;***](TLRolePermission.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


