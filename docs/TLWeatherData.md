# TLWeatherData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**weatherSummary** | **NSString*** | Weather Summary | [optional] 
**precipType** | **NSString*** | Precipitation Type | [optional] 
**apparentTemp** | **NSNumber*** | Apparent Temperature | [optional] 
**cloudCover** | **NSNumber*** | Cloud Cover | [optional] 
**dewPoint** | **NSNumber*** | Dew Point | [optional] 
**humidity** | **NSNumber*** | Humidity | [optional] 
**ozone** | **NSNumber*** | Ozone Rating | [optional] 
**precipIntensity** | **NSNumber*** | Precipitation Intesity | [optional] 
**precipProbability** | **NSNumber*** | Precipitation Probability | [optional] 
**pressure** | **NSNumber*** | Barometric Pressure | [optional] 
**temperature** | **NSNumber*** | Temperature | [optional] 
**time** | **NSNumber*** | Weather Sample Time (epoch) | [optional] 
**uvIndex** | **NSNumber*** | UV Index | [optional] 
**windBearing** | **NSNumber*** | Wind Bearing | [optional] 
**windGust** | **NSNumber*** | Wind Gust | [optional] 
**windSpeed** | **NSNumber*** | Wind Speed | [optional] 
**visibility** | **NSNumber*** | Visibility | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


