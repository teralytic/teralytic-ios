# TLTelemetryApi

All URIs are relative to *https://api.teralytic.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fieldAnalyticsQuery**](TLTelemetryApi.md#fieldanalyticsquery) | **GET** /organizations/{organization_id}/fields/{field_id}/analytics | Query field anlaytics
[**probeAnalyticsQuery**](TLTelemetryApi.md#probeanalyticsquery) | **GET** /organizations/{organization_id}/probes/{probe_id}/analytics | Query probe analytics
[**readingsQuery**](TLTelemetryApi.md#readingsquery) | **GET** /organizations/{organization_id}/readings | Query sensor readings


# **fieldAnalyticsQuery**
```objc
-(NSURLSessionTask*) fieldAnalyticsQueryWithOrganizationId: (NSString*) organizationId
    fieldId: (NSString*) fieldId
    startDate: (NSDate*) startDate
    endDate: (NSDate*) endDate
    operation: (NSString*) operation
    sampleRate: (NSString*) sampleRate
    limit: (NSNumber*) limit
    extended: (NSNumber*) extended
    sort: (NSNumber*) sort
        completionHandler: (void (^)(NSArray<TLReading>* output, NSError* error)) handler;
```

Query field anlaytics

List single Field details associated with Field id provided (from set of Fields associated with the account key)

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization to retrieve
NSString* fieldId = @"fieldId_example"; // id of Field to retrieve
NSDate* startDate = @"2013-10-20T19:20:30+01:00"; // Start date and time for the query in RFC3339 format (optional)
NSDate* endDate = @"2013-10-20T19:20:30+01:00"; // End date and time for the query in RFC3339 format (optional)
NSString* operation = @"mean"; // Operation to perform on reading data (optional) (default to mean)
NSString* sampleRate = @"15m"; // The operation sample interval, i.e. `15m` (optional) (default to 15m)
NSNumber* limit = @789; // limit the number of returns per depth (optional)
NSNumber* extended = @false; // Return extended attributes (optional) (default to false)
NSNumber* sort = @true; // return a sorted array, will decrease performance (optional) (default to true)

TLTelemetryApi*apiInstance = [[TLTelemetryApi alloc] init];

// Query field anlaytics
[apiInstance fieldAnalyticsQueryWithOrganizationId:organizationId
              fieldId:fieldId
              startDate:startDate
              endDate:endDate
              operation:operation
              sampleRate:sampleRate
              limit:limit
              extended:extended
              sort:sort
          completionHandler: ^(NSArray<TLReading>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLTelemetryApi->fieldAnalyticsQuery: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization to retrieve | 
 **fieldId** | [**NSString***](.md)| id of Field to retrieve | 
 **startDate** | **NSDate***| Start date and time for the query in RFC3339 format | [optional] 
 **endDate** | **NSDate***| End date and time for the query in RFC3339 format | [optional] 
 **operation** | **NSString***| Operation to perform on reading data | [optional] [default to mean]
 **sampleRate** | **NSString***| The operation sample interval, i.e. &#x60;15m&#x60; | [optional] [default to 15m]
 **limit** | **NSNumber***| limit the number of returns per depth | [optional] 
 **extended** | **NSNumber***| Return extended attributes | [optional] [default to false]
 **sort** | **NSNumber***| return a sorted array, will decrease performance | [optional] [default to true]

### Return type

[**NSArray<TLReading>***](TLReading.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **probeAnalyticsQuery**
```objc
-(NSURLSessionTask*) probeAnalyticsQueryWithOrganizationId: (NSString*) organizationId
    probeId: (NSString*) probeId
    startDate: (NSDate*) startDate
    endDate: (NSDate*) endDate
    operation: (NSString*) operation
    sampleRate: (NSString*) sampleRate
    limit: (NSNumber*) limit
    extended: (NSNumber*) extended
    sort: (NSNumber*) sort
        completionHandler: (void (^)(NSArray<TLReading>* output, NSError* error)) handler;
```

Query probe analytics

List single Probe details, including Reading data, associated with Probe id provided (from set of Fields associated with the account key)

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization to retrieve
NSString* probeId = @"probeId_example"; // id of Probe to retrieve
NSDate* startDate = @"2013-10-20T19:20:30+01:00"; // Start date and time for the query in RFC3339 format (optional)
NSDate* endDate = @"2013-10-20T19:20:30+01:00"; // End date and time for the query in RFC3339 format (optional)
NSString* operation = @"mean"; // Operation to perform on reading data (optional) (default to mean)
NSString* sampleRate = @"15m"; // The operation sample interval, i.e. `15m` (optional) (default to 15m)
NSNumber* limit = @789; // limit the number of returns per depth (optional)
NSNumber* extended = @false; // Return extended attributes (optional) (default to false)
NSNumber* sort = @true; // return a sorted array, will decrease performance (optional) (default to true)

TLTelemetryApi*apiInstance = [[TLTelemetryApi alloc] init];

// Query probe analytics
[apiInstance probeAnalyticsQueryWithOrganizationId:organizationId
              probeId:probeId
              startDate:startDate
              endDate:endDate
              operation:operation
              sampleRate:sampleRate
              limit:limit
              extended:extended
              sort:sort
          completionHandler: ^(NSArray<TLReading>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLTelemetryApi->probeAnalyticsQuery: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization to retrieve | 
 **probeId** | **NSString***| id of Probe to retrieve | 
 **startDate** | **NSDate***| Start date and time for the query in RFC3339 format | [optional] 
 **endDate** | **NSDate***| End date and time for the query in RFC3339 format | [optional] 
 **operation** | **NSString***| Operation to perform on reading data | [optional] [default to mean]
 **sampleRate** | **NSString***| The operation sample interval, i.e. &#x60;15m&#x60; | [optional] [default to 15m]
 **limit** | **NSNumber***| limit the number of returns per depth | [optional] 
 **extended** | **NSNumber***| Return extended attributes | [optional] [default to false]
 **sort** | **NSNumber***| return a sorted array, will decrease performance | [optional] [default to true]

### Return type

[**NSArray<TLReading>***](TLReading.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **readingsQuery**
```objc
-(NSURLSessionTask*) readingsQueryWithOrganizationId: (NSString*) organizationId
    startDate: (NSDate*) startDate
    endDate: (NSDate*) endDate
    fields: (NSArray<NSString*>*) fields
    probes: (NSArray<NSString*>*) probes
    properties: (NSArray<NSString*>*) properties
    extended: (NSNumber*) extended
        completionHandler: (void (^)(NSArray<TLReading>* output, NSError* error)) handler;
```

Query sensor readings

Query sensor readings associated with organization

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization to retrieve
NSDate* startDate = @"2013-10-20T19:20:30+01:00"; // Start date and time for the query in RFC3339 format, the default is 24h prior  (optional)
NSDate* endDate = @"2013-10-20T19:20:30+01:00"; // End date and time for the query in RFC3339 format, the implied default is now  (optional)
NSArray<NSString*>* fields = @[@"fields_example"]; // The fields to return readings for (optional)
NSArray<NSString*>* probes = @[@"probes_example"]; // The probes to return readings for (optional)
NSArray<NSString*>* properties = @[@"properties_example"]; // The properties to return readings for (optional)
NSNumber* extended = @false; // Return extended attributes (optional) (default to false)

TLTelemetryApi*apiInstance = [[TLTelemetryApi alloc] init];

// Query sensor readings
[apiInstance readingsQueryWithOrganizationId:organizationId
              startDate:startDate
              endDate:endDate
              fields:fields
              probes:probes
              properties:properties
              extended:extended
          completionHandler: ^(NSArray<TLReading>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLTelemetryApi->readingsQuery: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization to retrieve | 
 **startDate** | **NSDate***| Start date and time for the query in RFC3339 format, the default is 24h prior  | [optional] 
 **endDate** | **NSDate***| End date and time for the query in RFC3339 format, the implied default is now  | [optional] 
 **fields** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| The fields to return readings for | [optional] 
 **probes** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| The probes to return readings for | [optional] 
 **properties** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| The properties to return readings for | [optional] 
 **extended** | **NSNumber***| Return extended attributes | [optional] [default to false]

### Return type

[**NSArray<TLReading>***](TLReading.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

