# TLOrganizationApi

All URIs are relative to *https://api.teralytic.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**applicationCreate**](TLOrganizationApi.md#applicationcreate) | **POST** /organizations/{organization_id}/applications | Create a new organization client with the specified scope
[**applicationDelete**](TLOrganizationApi.md#applicationdelete) | **DELETE** /organizations/{organization_id}/applications/{application_id} | Delete an application api client
[**applicationList**](TLOrganizationApi.md#applicationlist) | **GET** /organizations/{organization_id}/applications | Get organization applications
[**fieldGet**](TLOrganizationApi.md#fieldget) | **GET** /organizations/{organization_id}/fields/{field_id} | List single Field details associated with Field id provided (from set of Fields associated with the organization)
[**fieldList**](TLOrganizationApi.md#fieldlist) | **GET** /organizations/{organization_id}/fields | List all Fields associated with an organization
[**organizationGet**](TLOrganizationApi.md#organizationget) | **GET** /organizations/{organization_id} | Get a specific organization
[**organizationList**](TLOrganizationApi.md#organizationlist) | **GET** /organizations | List all Organizations
[**probeGet**](TLOrganizationApi.md#probeget) | **GET** /organizations/{organization_id}/probes/{probe_id} | List single Probe details associated with Probe id provided (from set of Fields associated with the account key)
[**probeList**](TLOrganizationApi.md#probelist) | **GET** /organizations/{organization_id}/probes | List all Probes associated with an organization


# **applicationCreate**
```objc
-(NSURLSessionTask*) applicationCreateWithOrganizationId: (NSString*) organizationId
    application: (TLApplication*) application
        completionHandler: (void (^)(TLApplication* output, NSError* error)) handler;
```

Create a new organization client with the specified scope

Create a new third-party application client

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization for the operation
TLApplication* application = [[TLApplication alloc] init]; // The application to create (optional)

TLOrganizationApi*apiInstance = [[TLOrganizationApi alloc] init];

// Create a new organization client with the specified scope
[apiInstance applicationCreateWithOrganizationId:organizationId
              application:application
          completionHandler: ^(TLApplication* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLOrganizationApi->applicationCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization for the operation | 
 **application** | [**TLApplication***](TLApplication.md)| The application to create | [optional] 

### Return type

[**TLApplication***](TLApplication.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **applicationDelete**
```objc
-(NSURLSessionTask*) applicationDeleteWithOrganizationId: (NSString*) organizationId
    applicationId: (NSString*) applicationId
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete an application api client

Delete an application client for the organization from the database

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization for the operation
NSString* applicationId = @"applicationId_example"; // id of application to delete

TLOrganizationApi*apiInstance = [[TLOrganizationApi alloc] init];

// Delete an application api client
[apiInstance applicationDeleteWithOrganizationId:organizationId
              applicationId:applicationId
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling TLOrganizationApi->applicationDelete: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization for the operation | 
 **applicationId** | [**NSString***](.md)| id of application to delete | 

### Return type

void (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **applicationList**
```objc
-(NSURLSessionTask*) applicationListWithOrganizationId: (NSString*) organizationId
        completionHandler: (void (^)(NSArray<TLApplication>* output, NSError* error)) handler;
```

Get organization applications

List all applications for an organization

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization for the operation

TLOrganizationApi*apiInstance = [[TLOrganizationApi alloc] init];

// Get organization applications
[apiInstance applicationListWithOrganizationId:organizationId
          completionHandler: ^(NSArray<TLApplication>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLOrganizationApi->applicationList: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization for the operation | 

### Return type

[**NSArray<TLApplication>***](TLApplication.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fieldGet**
```objc
-(NSURLSessionTask*) fieldGetWithOrganizationId: (NSString*) organizationId
    fieldId: (NSString*) fieldId
        completionHandler: (void (^)(TLField* output, NSError* error)) handler;
```

List single Field details associated with Field id provided (from set of Fields associated with the organization)

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization to retrieve
NSString* fieldId = @"fieldId_example"; // id of Field to retrieve

TLOrganizationApi*apiInstance = [[TLOrganizationApi alloc] init];

// List single Field details associated with Field id provided (from set of Fields associated with the organization)
[apiInstance fieldGetWithOrganizationId:organizationId
              fieldId:fieldId
          completionHandler: ^(TLField* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLOrganizationApi->fieldGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization to retrieve | 
 **fieldId** | [**NSString***](.md)| id of Field to retrieve | 

### Return type

[**TLField***](TLField.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fieldList**
```objc
-(NSURLSessionTask*) fieldListWithOrganizationId: (NSString*) organizationId
    points: (NSArray<NSString*>*) points
        completionHandler: (void (^)(NSArray<TLField>* output, NSError* error)) handler;
```

List all Fields associated with an organization

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization to retrieve
NSArray<NSString*>* points = @[@"points_example"]; // Array of points (lat,lng) describing a polygon search pattern. (optional)

TLOrganizationApi*apiInstance = [[TLOrganizationApi alloc] init];

// List all Fields associated with an organization
[apiInstance fieldListWithOrganizationId:organizationId
              points:points
          completionHandler: ^(NSArray<TLField>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLOrganizationApi->fieldList: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization to retrieve | 
 **points** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Array of points (lat,lng) describing a polygon search pattern. | [optional] 

### Return type

[**NSArray<TLField>***](TLField.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **organizationGet**
```objc
-(NSURLSessionTask*) organizationGetWithOrganizationId: (NSString*) organizationId
        completionHandler: (void (^)(TLOrganization* output, NSError* error)) handler;
```

Get a specific organization

List single Organization details associated with Organization id provided

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization to retrieve

TLOrganizationApi*apiInstance = [[TLOrganizationApi alloc] init];

// Get a specific organization
[apiInstance organizationGetWithOrganizationId:organizationId
          completionHandler: ^(TLOrganization* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLOrganizationApi->organizationGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization to retrieve | 

### Return type

[**TLOrganization***](TLOrganization.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **organizationList**
```objc
-(NSURLSessionTask*) organizationListWithCompletionHandler: 
        (void (^)(NSArray<TLOrganization>* output, NSError* error)) handler;
```

List all Organizations

Lists all organization, requires admin scope

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];



TLOrganizationApi*apiInstance = [[TLOrganizationApi alloc] init];

// List all Organizations
[apiInstance organizationListWithCompletionHandler: 
          ^(NSArray<TLOrganization>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLOrganizationApi->organizationList: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<TLOrganization>***](TLOrganization.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **probeGet**
```objc
-(NSURLSessionTask*) probeGetWithOrganizationId: (NSString*) organizationId
    probeId: (NSString*) probeId
        completionHandler: (void (^)(TLProbe* output, NSError* error)) handler;
```

List single Probe details associated with Probe id provided (from set of Fields associated with the account key)

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization to retrieve
NSString* probeId = @"probeId_example"; // id of Probe to retrieve

TLOrganizationApi*apiInstance = [[TLOrganizationApi alloc] init];

// List single Probe details associated with Probe id provided (from set of Fields associated with the account key)
[apiInstance probeGetWithOrganizationId:organizationId
              probeId:probeId
          completionHandler: ^(TLProbe* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLOrganizationApi->probeGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization to retrieve | 
 **probeId** | **NSString***| id of Probe to retrieve | 

### Return type

[**TLProbe***](TLProbe.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **probeList**
```objc
-(NSURLSessionTask*) probeListWithOrganizationId: (NSString*) organizationId
    fields: (NSArray<NSString*>*) fields
        completionHandler: (void (^)(NSArray<TLProbe>* output, NSError* error)) handler;
```

List all Probes associated with an organization

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization to retrieve
NSArray<NSString*>* fields = @[@"fields_example"]; // Fields to filter search on (optional)

TLOrganizationApi*apiInstance = [[TLOrganizationApi alloc] init];

// List all Probes associated with an organization
[apiInstance probeListWithOrganizationId:organizationId
              fields:fields
          completionHandler: ^(NSArray<TLProbe>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLOrganizationApi->probeList: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization to retrieve | 
 **fields** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Fields to filter search on | [optional] 

### Return type

[**NSArray<TLProbe>***](TLProbe.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

