# TLReading

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSString*** | Base58 encoded identifier for this reading. Corresponds with a single Probe and single timestamp. | [optional] 
**probeId** | **NSString*** | id of probe from which Reading was made | [optional] 
**probeName** | **NSString*** | Name of probe from which Reading was made | [optional] 
**fieldId** | **NSString*** | The field id of the probe | [optional] 
**timestamp** | **NSString*** | Time when Reading was measured | [optional] 
**location** | [**TLReadingLocation***](TLReadingLocation.md) |  | [optional] 
**operation** | **NSString*** | The analytics operation | [optional] 
**readings** | [**NSArray&lt;TLReadingData&gt;***](TLReadingData.md) | Probe readings | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


