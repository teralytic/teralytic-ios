# TLFieldApi

All URIs are relative to *https://api.teralytic.io/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fieldGet**](TLFieldApi.md#fieldget) | **GET** /organizations/{organization_id}/fields/{field_id} | List single Field details associated with Field id provided (from set of Fields associated with the account key)
[**fieldList**](TLFieldApi.md#fieldlist) | **GET** /organizations/{organization_id}/fields | List all Fields associated with account key


# **fieldGet**
```objc
-(NSURLSessionTask*) fieldGetWithOrganizationId: (NSString*) organizationId
    fieldId: (NSString*) fieldId
        completionHandler: (void (^)(TLField* output, NSError* error)) handler;
```

List single Field details associated with Field id provided (from set of Fields associated with the account key)

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization to retrieve
NSString* fieldId = @"fieldId_example"; // id of Field to retrieve

TLFieldApi*apiInstance = [[TLFieldApi alloc] init];

// List single Field details associated with Field id provided (from set of Fields associated with the account key)
[apiInstance fieldGetWithOrganizationId:organizationId
              fieldId:fieldId
          completionHandler: ^(TLField* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLFieldApi->fieldGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization to retrieve | 
 **fieldId** | [**NSString***](.md)| id of Field to retrieve | 

### Return type

[**TLField***](TLField.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fieldList**
```objc
-(NSURLSessionTask*) fieldListWithOrganizationId: (NSString*) organizationId
    points: (NSArray<NSString*>*) points
        completionHandler: (void (^)(NSArray<TLField>* output, NSError* error)) handler;
```

List all Fields associated with account key

### Example 
```objc
TLDefaultConfiguration *apiConfig = [TLDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: ApiKeyAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"x-api-key"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"x-api-key"];

// Configure OAuth2 access token for authorization: (authentication scheme: OAuth2)
[apiConfig setAccessToken:@"YOUR_ACCESS_TOKEN"];


NSString* organizationId = @"organizationId_example"; // id of Organization to retrieve
NSArray<NSString*>* points = @[@"points_example"]; // Array of points (lat,lng) describing a polygon search pattern. (optional)

TLFieldApi*apiInstance = [[TLFieldApi alloc] init];

// List all Fields associated with account key
[apiInstance fieldListWithOrganizationId:organizationId
              points:points
          completionHandler: ^(NSArray<TLField>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling TLFieldApi->fieldList: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organizationId** | [**NSString***](.md)| id of Organization to retrieve | 
 **points** | [**NSArray&lt;NSString*&gt;***](NSString*.md)| Array of points (lat,lng) describing a polygon search pattern. | [optional] 

### Return type

[**NSArray<TLField>***](TLField.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

