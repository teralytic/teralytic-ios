# TLRolePermission

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSString*** | The role id | [optional] 
**domain** | **NSString*** | The role domain | [optional] 
**action** | **NSString*** | The role action | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


