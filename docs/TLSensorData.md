# TLSensorData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**depth** | **NSNumber*** | The depth value | [optional] 
**depthUnits** | **NSString*** | The depth units (i.e. in, cm) | [optional] 
**terascore** | **NSNumber*** | TeraScore | [optional] 
**nitrogen** | **NSNumber*** | Nitrogen | [optional] 
**nitrogenPpm** | **NSNumber*** | Nitrogen ppm | [optional] 
**phosphorus** | **NSNumber*** | Phosphorus | [optional] 
**phosphorusPpm** | **NSNumber*** | Phosphorus ppm | [optional] 
**potassium** | **NSNumber*** | Potassium | [optional] 
**potassiumPpm** | **NSNumber*** | Potassium ppm | [optional] 
**ec** | **NSNumber*** | Electrical Conductivity | [optional] 
**o2** | **NSNumber*** | Dioxygen | [optional] 
**pH** | **NSNumber*** | pH Scale | [optional] 
**co2** | **NSNumber*** | Carbon Dioxide | [optional] 
**awc** | **NSNumber*** |  | [optional] 
**soilMoisture** | **NSNumber*** | Soil Moisture | [optional] 
**soilTemperature** | **NSNumber*** | Soil Temperature | [optional] 
**soilTexture** | **NSString*** | Soil texture type. Data are collected from government data sources (USDA&#39;s SSURGO database for fields in the United States) | [optional] 
**extendedAttributes** | **NSDictionary&lt;NSString*, NSObject*&gt;*** | Additional properties | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


