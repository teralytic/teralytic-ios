# TLField

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSString*** |  | 
**organizationId** | **NSString*** |  | [optional] 
**name** | **NSString*** |  | 
**acreage** | **NSNumber*** |  | [optional] 
**crop** | **NSString*** |  | [optional] 
**geometry** | [**TLFieldGeometry***](TLFieldGeometry.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


