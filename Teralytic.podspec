#
# Be sure to run `pod lib lint Teralytic.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = "Teralytic"
    s.version          = "1.0.6"

    s.summary          = "teralytic"
    s.description      = <<-DESC
                         The Teralytic API allows clients to manage their organization, view their fields and and probes, and query sensor readings and analytics.  For sandbox testing you may use the api key: `swagger.teralytic.io` 
                         DESC

    s.platform     = :ios, '7.0'
    s.requires_arc = true

    s.framework    = 'SystemConfiguration'

    s.homepage     = "https://gitlab.com/teralytic/teralytic-ios"
    s.license      = "Proprietary"
    s.source       = { :git => "https://gitlab.com/teralytic/teralytic-ios.git", :tag => "#{s.version}" }
    s.author       = { "Teralytic" => "support@teralytic.com" }

    s.source_files = 'Teralytic/**/*.{m,h}'
    s.public_header_files = 'Teralytic/**/*.h'


    s.dependency 'AFNetworking', '~> 3'
    s.dependency 'JSONModel', '~> 1.2'
    s.dependency 'ISO8601', '~> 0.6'
end

