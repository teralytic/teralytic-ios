#import "TLWeatherData.h"

@implementation TLWeatherData

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"weatherSummary": @"weather_summary", @"precipType": @"precip_type", @"apparentTemp": @"apparent_temp", @"cloudCover": @"cloud_cover", @"dewPoint": @"dew_point", @"humidity": @"humidity", @"ozone": @"ozone", @"precipIntensity": @"precip_intensity", @"precipProbability": @"precip_probability", @"pressure": @"pressure", @"temperature": @"temperature", @"time": @"time", @"uvIndex": @"uv_index", @"windBearing": @"wind_bearing", @"windGust": @"wind_gust", @"windSpeed": @"wind_speed", @"visibility": @"visibility" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"weatherSummary", @"precipType", @"apparentTemp", @"cloudCover", @"dewPoint", @"humidity", @"ozone", @"precipIntensity", @"precipProbability", @"pressure", @"temperature", @"time", @"uvIndex", @"windBearing", @"windGust", @"windSpeed", @"visibility"];
  return [optionalProperties containsObject:propertyName];
}

@end
