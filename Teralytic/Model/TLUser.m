#import "TLUser.h"

@implementation TLUser

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"organizations": @"organizations", @"firstName": @"first_name", @"lastName": @"last_name", @"email": @"email", @"emailVerified": @"email_verified", @"username": @"username", @"orgRoles": @"org_roles", @"propertyRoles": @"property_roles" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"organizations", @"firstName", @"lastName", @"email", @"emailVerified", @"username", @"orgRoles", @"propertyRoles"];
  return [optionalProperties containsObject:propertyName];
}

@end
