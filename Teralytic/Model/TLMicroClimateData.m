#import "TLMicroClimateData.h"

@implementation TLMicroClimateData

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"humidity": @"humidity", @"irrigationSched": @"irrigation_sched", @"irrigationVolAcreIn": @"irrigation_vol_acre_in", @"irrigationVolAcreCm": @"irrigation_vol_acre_cm", @"lux": @"lux", @"irrigationGrossIn": @"irrigation_gross_in", @"irrigationNetIn": @"irrigation_net_in", @"irrigationGrossCm": @"irrigation_gross_cm", @"irrigationNetCm": @"irrigation_net_cm", @"irrigationRec": @"irrigation_rec", @"temperature": @"temperature", @"eto": @"eto", @"irrigationStream": @"irrigation_stream", @"organicMatter": @"organic_matter", @"awIn6": @"aw_in6", @"awIn18": @"aw_in18", @"awIn36": @"aw_in36", @"awTotal": @"aw_total" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"humidity", @"irrigationSched", @"irrigationVolAcreIn", @"irrigationVolAcreCm", @"lux", @"irrigationGrossIn", @"irrigationNetIn", @"irrigationGrossCm", @"irrigationNetCm", @"irrigationRec", @"temperature", @"eto", @"irrigationStream", @"organicMatter", @"awIn6", @"awIn18", @"awIn36", @"awTotal"];
  return [optionalProperties containsObject:propertyName];
}

@end
