#import "TLReadingsApi.h"
#import "TLQueryParamCollection.h"
#import "TLApiClient.h"
#import "TLError.h"
#import "TLReading.h"


@interface TLReadingsApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation TLReadingsApi

NSString* kTLReadingsApiErrorDomain = @"TLReadingsApiErrorDomain";
NSInteger kTLReadingsApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[TLApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(TLApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// Query reading anlaytics
/// Queries readings and performs anayltics operations on the them over a sample
///  @param organizationId id of Organization to retrieve 
///
///  @param startDate Start date and time for the query in RFC3339 format (optional)
///
///  @param endDate End date and time for the query in RFC3339 format (optional)
///
///  @param fields The fields to return readings for (optional)
///
///  @param probes The probes to return readings for (optional)
///
///  @param properties The properties to return readings for (optional)
///
///  @param operation Operation to perform on reading data (optional, default to mean)
///
///  @param sampleRate The operation sample interval, i.e. `15m` (optional, default to 15m)
///
///  @param limit limit the number of returns per depth (optional)
///
///  @param extended Return extended attributes (optional, default to false)
///
///  @param sort return a sorted array, will decrease performance (optional, default to true)
///
///  @returns NSArray<TLReading>*
///
-(NSURLSessionTask*) analyticsQueryWithOrganizationId: (NSString*) organizationId
    startDate: (NSDate*) startDate
    endDate: (NSDate*) endDate
    fields: (NSArray<NSString*>*) fields
    probes: (NSArray<NSString*>*) probes
    properties: (NSArray<NSString*>*) properties
    operation: (NSString*) operation
    sampleRate: (NSString*) sampleRate
    limit: (NSNumber*) limit
    extended: (NSNumber*) extended
    sort: (NSNumber*) sort
    completionHandler: (void (^)(NSArray<TLReading>* output, NSError* error)) handler {
    // verify the required parameter 'organizationId' is set
    if (organizationId == nil) {
        NSParameterAssert(organizationId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"organizationId"] };
            NSError* error = [NSError errorWithDomain:kTLReadingsApiErrorDomain code:kTLReadingsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/organizations/{organization_id}/analytics"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (organizationId != nil) {
        pathParams[@"organization_id"] = organizationId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (startDate != nil) {
        queryParams[@"start_date"] = startDate;
    }
    if (endDate != nil) {
        queryParams[@"end_date"] = endDate;
    }
    if (fields != nil) {
        queryParams[@"fields"] = [[TLQueryParamCollection alloc] initWithValuesAndFormat: fields format: @"csv"];
    }
    if (probes != nil) {
        queryParams[@"probes"] = [[TLQueryParamCollection alloc] initWithValuesAndFormat: probes format: @"csv"];
    }
    if (properties != nil) {
        queryParams[@"properties"] = [[TLQueryParamCollection alloc] initWithValuesAndFormat: properties format: @"csv"];
    }
    if (operation != nil) {
        queryParams[@"operation"] = operation;
    }
    if (sampleRate != nil) {
        queryParams[@"sample_rate"] = sampleRate;
    }
    if (limit != nil) {
        queryParams[@"limit"] = limit;
    }
    if (extended != nil) {
        queryParams[@"extended"] = [extended isEqual:@(YES)] ? @"true" : @"false";
    }
    if (sort != nil) {
        queryParams[@"sort"] = [sort isEqual:@(YES)] ? @"true" : @"false";
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"ApiKeyAuth", @"OAuth2"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<TLReading>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<TLReading>*)data, error);
                                }
                            }];
}

///
/// Query sensor readings
/// Query sensor readings associated with organization
///  @param organizationId id of Organization to retrieve 
///
///  @param startDate Start date and time for the query in RFC3339 format, the default is 24h prior  (optional)
///
///  @param endDate End date and time for the query in RFC3339 format, the implied default is now  (optional)
///
///  @param fields The fields to return readings for (optional)
///
///  @param probes The probes to return readings for (optional)
///
///  @param properties The properties to return readings for (optional)
///
///  @param extended Return extended attributes (optional, default to false)
///
///  @returns NSArray<TLReading>*
///
-(NSURLSessionTask*) readingsQueryWithOrganizationId: (NSString*) organizationId
    startDate: (NSDate*) startDate
    endDate: (NSDate*) endDate
    fields: (NSArray<NSString*>*) fields
    probes: (NSArray<NSString*>*) probes
    properties: (NSArray<NSString*>*) properties
    extended: (NSNumber*) extended
    completionHandler: (void (^)(NSArray<TLReading>* output, NSError* error)) handler {
    // verify the required parameter 'organizationId' is set
    if (organizationId == nil) {
        NSParameterAssert(organizationId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"organizationId"] };
            NSError* error = [NSError errorWithDomain:kTLReadingsApiErrorDomain code:kTLReadingsApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/organizations/{organization_id}/readings"];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (organizationId != nil) {
        pathParams[@"organization_id"] = organizationId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (startDate != nil) {
        queryParams[@"start_date"] = startDate;
    }
    if (endDate != nil) {
        queryParams[@"end_date"] = endDate;
    }
    if (fields != nil) {
        queryParams[@"fields"] = [[TLQueryParamCollection alloc] initWithValuesAndFormat: fields format: @"csv"];
    }
    if (probes != nil) {
        queryParams[@"probes"] = [[TLQueryParamCollection alloc] initWithValuesAndFormat: probes format: @"csv"];
    }
    if (properties != nil) {
        queryParams[@"properties"] = [[TLQueryParamCollection alloc] initWithValuesAndFormat: properties format: @"csv"];
    }
    if (extended != nil) {
        queryParams[@"extended"] = [extended isEqual:@(YES)] ? @"true" : @"false";
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"ApiKeyAuth", @"OAuth2"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"NSArray<TLReading>*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((NSArray<TLReading>*)data, error);
                                }
                            }];
}



@end
