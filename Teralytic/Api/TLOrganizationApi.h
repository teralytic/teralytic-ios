#import <Foundation/Foundation.h>
#import "TLApplication.h"
#import "TLError.h"
#import "TLField.h"
#import "TLOrganization.h"
#import "TLProbe.h"
#import "TLApi.h"

/**
* teralytic
* The Teralytic API allows clients to manage their organization, view their fields and and probes, and query sensor readings and analytics.  For sandbox testing you may use the api key: `swagger.teralytic.io` 
*
* OpenAPI spec version: 1.0.6
* 
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/



@interface TLOrganizationApi: NSObject <TLApi>

extern NSString* kTLOrganizationApiErrorDomain;
extern NSInteger kTLOrganizationApiMissingParamErrorCode;

-(instancetype) initWithApiClient:(TLApiClient *)apiClient NS_DESIGNATED_INITIALIZER;

/// Create a new organization client with the specified scope
/// Create a new third-party application client
///
/// @param organizationId id of Organization for the operation
/// @param application The application to create (optional)
/// 
///  code:201 message:"Created",
///  code:400 message:"BadRequest",
///  code:401 message:"Unauthorized",
///  code:404 message:"ResourceNotFound",
///  code:500 message:"ServerError"
///
/// @return TLApplication*
-(NSURLSessionTask*) applicationCreateWithOrganizationId: (NSString*) organizationId
    application: (TLApplication*) application
    completionHandler: (void (^)(TLApplication* output, NSError* error)) handler;


/// Delete an application api client
/// Delete an application client for the organization from the database
///
/// @param organizationId id of Organization for the operation
/// @param applicationId id of application to delete
/// 
///  code:204 message:"No Content",
///  code:401 message:"Unauthorized",
///  code:404 message:"ResourceNotFound",
///  code:500 message:"ServerError"
///
/// @return void
-(NSURLSessionTask*) applicationDeleteWithOrganizationId: (NSString*) organizationId
    applicationId: (NSString*) applicationId
    completionHandler: (void (^)(NSError* error)) handler;


/// Get organization applications
/// List all applications for an organization
///
/// @param organizationId id of Organization for the operation
/// 
///  code:200 message:"OK",
///  code:401 message:"Unauthorized",
///  code:404 message:"ResourceNotFound",
///  code:500 message:"ServerError"
///
/// @return NSArray<TLApplication>*
-(NSURLSessionTask*) applicationListWithOrganizationId: (NSString*) organizationId
    completionHandler: (void (^)(NSArray<TLApplication>* output, NSError* error)) handler;


/// List single Field details associated with Field id provided (from set of Fields associated with the organization)
/// 
///
/// @param organizationId id of Organization to retrieve
/// @param fieldId id of Field to retrieve
/// 
///  code:200 message:"OK",
///  code:401 message:"Unauthorized",
///  code:404 message:"ResourceNotFound",
///  code:500 message:"ServerError"
///
/// @return TLField*
-(NSURLSessionTask*) fieldGetWithOrganizationId: (NSString*) organizationId
    fieldId: (NSString*) fieldId
    completionHandler: (void (^)(TLField* output, NSError* error)) handler;


/// List all Fields associated with an organization
/// 
///
/// @param organizationId id of Organization to retrieve
/// @param points Array of points (lat,lng) describing a polygon search pattern. (optional)
/// 
///  code:200 message:"OK",
///  code:400 message:"BadRequest",
///  code:401 message:"Unauthorized",
///  code:404 message:"ResourceNotFound",
///  code:500 message:"ServerError"
///
/// @return NSArray<TLField>*
-(NSURLSessionTask*) fieldListWithOrganizationId: (NSString*) organizationId
    points: (NSArray<NSString*>*) points
    completionHandler: (void (^)(NSArray<TLField>* output, NSError* error)) handler;


/// Get a specific organization
/// List single Organization details associated with Organization id provided
///
/// @param organizationId id of Organization to retrieve
/// 
///  code:200 message:"OK",
///  code:401 message:"Unauthorized",
///  code:404 message:"ResourceNotFound",
///  code:500 message:"ServerError"
///
/// @return TLOrganization*
-(NSURLSessionTask*) organizationGetWithOrganizationId: (NSString*) organizationId
    completionHandler: (void (^)(TLOrganization* output, NSError* error)) handler;


/// List all Organizations
/// Lists all organization, requires admin scope
///
/// 
///  code:200 message:"Success",
///  code:401 message:"Unauthorized",
///  code:500 message:"ServerError"
///
/// @return NSArray<TLOrganization>*
-(NSURLSessionTask*) organizationListWithCompletionHandler: 
    (void (^)(NSArray<TLOrganization>* output, NSError* error)) handler;


/// List single Probe details associated with Probe id provided (from set of Fields associated with the account key)
/// 
///
/// @param organizationId id of Organization to retrieve
/// @param probeId id of Probe to retrieve
/// 
///  code:200 message:"OK",
///  code:401 message:"Unauthorized",
///  code:404 message:"ResourceNotFound",
///  code:500 message:"ServerError"
///
/// @return TLProbe*
-(NSURLSessionTask*) probeGetWithOrganizationId: (NSString*) organizationId
    probeId: (NSString*) probeId
    completionHandler: (void (^)(TLProbe* output, NSError* error)) handler;


/// List all Probes associated with an organization
/// 
///
/// @param organizationId id of Organization to retrieve
/// @param fields Fields to filter search on (optional)
/// 
///  code:200 message:"OK",
///  code:400 message:"BadRequest",
///  code:401 message:"Unauthorized",
///  code:404 message:"ResourceNotFound",
///  code:500 message:"ServerError"
///
/// @return NSArray<TLProbe>*
-(NSURLSessionTask*) probeListWithOrganizationId: (NSString*) organizationId
    fields: (NSArray<NSString*>*) fields
    completionHandler: (void (^)(NSArray<TLProbe>* output, NSError* error)) handler;



@end
