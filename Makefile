VERSION := `npm view teralytic version`
GITLAB_PROJECT := 8558474
SWAGGER_CODEGEN ?= docker run --rm -v ${PWD}:/local swaggerapi/swagger-codegen-cli:2.4.0
SWAGGER_SPEC ?= swagger.yaml
GRUNT_REL ?= prerelease

default: publish

bump: 
	podspec-bump $(GRUNT_REL) -w
	grunt bump-only:$(GRUNT_REL)

fetch: bump
	curl -o swagger.yaml --header 'PRIVATE-TOKEN: $(GITLAB_TOKEN)' https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT)/repository/files/api%2Fteralytic.yaml/raw?ref=develop

client: fetch
	rm -fr client/dist/*
	$(SWAGGER_CODEGEN) generate -l objc -i /local/$(SWAGGER_SPEC) -c /local/config.json -o /local

commit: client
	grunt bump-commit

publish: commit
	pod trunk push Teralytic.podspec --allow-warnings

.PHONY: \
	client
